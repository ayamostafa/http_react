import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import axios from "axios";
axios.defaults.baseURL = "https://jsonplaceholder.typicode.com";
axios.defaults.headers.common["Authorization"] = "Auth Aya";
axios.defaults.headers.post["Content-Type"] = "Application/json";
// adding interceptors
var myInterceptor = axios.interceptors.request.use(
  (request) => {
    console.log(request);
    //Edit Request config here
    return request;
  },
  (error) => {
    console.log(error);
    return Promise.reject(error);
  }
);
// removing interceptors
axios.interceptors.request.eject(myInterceptor);
ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
